package com.twuc.webApp;

public class SimpleDependent {
    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public String getName() {
        return name;
    }

    public SimpleDependent() {
    }
}
