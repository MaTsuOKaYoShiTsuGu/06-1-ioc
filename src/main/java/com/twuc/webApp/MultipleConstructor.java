package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MultipleConstructor {
    private Dependency dependency;
    private String string;

    public MultipleConstructor(String string) {
        this.string = string;
    }

    @Autowired
    public MultipleConstructor(Dependency dependency) {
        this.dependency = dependency;
    }

    public String getString() {
        return string;
    }

    public Dependency getDependency() {
        return dependency;
    }
}
