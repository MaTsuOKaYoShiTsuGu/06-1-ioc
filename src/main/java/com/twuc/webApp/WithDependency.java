package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class WithDependency {
    private Dependency dependency;

    public WithDependency(Dependency dependency) {
        this.dependency = dependency;
    }

    public Dependency getDependency() {
        return dependency;
    }
}
