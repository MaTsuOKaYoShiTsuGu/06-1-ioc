package com.twuc.webApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class WithAutowiredMethod {
    private Dependency dependency;
    private AnotherDependent anotherDependent;
    private ArrayList<String> log = new ArrayList<>();
    private ArrayList<Boolean> paramLog = new ArrayList<>();
    public Dependency getDependency() {
        return dependency;
    }

    public WithAutowiredMethod(Dependency dependency) {
        this.dependency = dependency;
        log.add("dependency");
        paramLog.add(dependency == null);
    }

    @Autowired
    public void initialize(AnotherDependent anotherDependent){
        this.anotherDependent = anotherDependent;
        log.add("anotherDependent");
        paramLog.add(anotherDependent == null);
    }

    public AnotherDependent getAnotherDependent() {
        return anotherDependent;
    }

    public ArrayList<String> getLog() {
        return log;
    }

    public ArrayList<Boolean> getParamLog() {
        return paramLog;
    }
}
