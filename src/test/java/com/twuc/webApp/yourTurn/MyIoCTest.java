package com.twuc.webApp.yourTurn;


import com.twuc.webApp.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import out.of.scanning.scope.OutOfScanningScope;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class MyIoCTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void createContext(){
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_create_object_without_dependency() {
        WithoutDependency bean = context.getBean(WithoutDependency.class);
        assertNotNull(bean);
        assertEquals(bean.getClass(),WithoutDependency.class);
    }

    @Test
    void should_create_object_with_dependency() {
        WithDependency bean = context.getBean(WithDependency.class);
        assertNotNull(bean);
        assertNotNull(bean.getDependency());
    }

    @Test
    void should_failed_to_create_object_out_of_scanning_scope() {
        assertThrows(Exception.class,() -> {
            OutOfScanningScope bean = context.getBean(OutOfScanningScope.class);
        });
    }

    @Test
    void should_create_object_using_implement() {
        Interface bean = context.getBean(Interface.class);
        assertNotNull(bean);
    }

    @Test
    void should_get_simple_dependent_object_with_name_O_o() {
        SimpleObject bean = (SimpleObject) context.getBean(SimpleInterface.class);
        assertEquals("O_o",bean.getSimpleDependent().getName());
    }

    @Test
    void should_call_constructor_with_object_param() {
        MultipleConstructor bean = context.getBean(MultipleConstructor.class);
        assertNotNull(bean.getDependency());
    }

    @Test
    void should_call_constructor_first_and_params_are_not_null() {
        WithAutowiredMethod bean = context.getBean(WithAutowiredMethod.class);
        assertNotNull(bean.getDependency());
        assertNotNull(bean.getAnotherDependent());
        ArrayList<String> expect = new ArrayList<>();
        expect.add("dependency");
        expect.add("anotherDependent");
        assertIterableEquals(expect,bean.getLog());
        ArrayList<Boolean> exceptParam = new ArrayList<>();
        exceptParam.add(false);
        exceptParam.add(false);
        assertIterableEquals(exceptParam,bean.getParamLog());
    }

    @Test
    void should_implement_multiple_interfaces() {
        Map<String, InterfaceWithMultipleImpls> beans = context.getBeansOfType(InterfaceWithMultipleImpls.class);
        Set<String> keySet= beans.keySet();
        Object[] result = keySet.toArray();
        assertEquals(3,result.length);
        assertTrue(keySet.contains("implementationA"));
        assertTrue(keySet.contains("implementationB"));
        assertTrue(keySet.contains("implementationC"));
    }
}
